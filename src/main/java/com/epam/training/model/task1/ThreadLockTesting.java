package com.epam.training.model.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadLockTesting {
    private final Logger logger = LogManager.getLogger(ThreadLockTesting.class.getName());
    private static final ReentrantLock lock = new ReentrantLock();
    private static final ReentrantLock anotherLock = new ReentrantLock();
    private int incrementBy = 0;
    private int decrementByMinus = 0;
    private int sqr = 2;
    private int cycles;


    public ThreadLockTesting() {
        cycles = Integer.parseInt(ResourceBundle.getBundle("values")
                .getString("CyclesInLockTesting"));
    }

    public class FirstThread implements Runnable {
        @Override
        public void run() {
            methodInc();
        }
    }

    public class SecondThread implements Runnable {
        @Override
        public void run() {
            methodDec();
        }
    }

    public class ThirdThread implements Runnable {
        @Override
        public void run() {
            methodSqr();
        }
    }

    public void runThreeThreads() {
        Thread first = new Thread(new FirstThread());
        Thread second = new Thread(new SecondThread());
        Thread third = new Thread(new ThirdThread());
        first.start();
        second.start();
        third.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            logger.warn("Interrupt third thread");
        }

    }

    private void methodInc() {
        lock.lock();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        for (int i = 0; i < cycles; i++) {
            incrementBy++;
        }
        logger.info(Thread.currentThread().getName() + " incrementing");
        logger.info(Thread.currentThread().getName() + " -> " + incrementBy
                + " |" + LocalTime.now());
        lock.unlock();
    }

    private void methodDec() {
        anotherLock.lock();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        for (int i = 0; i < cycles; i++) {
            decrementByMinus--;
        }
        logger.info(Thread.currentThread().getName() + " decrementing");
        logger.info(Thread.currentThread().getName() + " -> " + decrementByMinus
                + " |" + LocalTime.now());
        anotherLock.unlock();
    }

    private void methodSqr() {
        lock.lock();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        sqr *= sqr;
        logger.info(Thread.currentThread().getName() + " sqr");
        logger.info(Thread.currentThread().getName() + " -> " + sqr + " |" + LocalTime.now());
        lock.unlock();
    }

}
