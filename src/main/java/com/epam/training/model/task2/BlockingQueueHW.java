package com.epam.training.model.task2;

import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueHW {

    BlockingQueue<Integer> bQueue;

    public BlockingQueueHW() {
        bQueue = new ArrayBlockingQueue<>(1, true);
    }

    public void runThreads() {
        Thread prodThread = new Thread(new Producer());
        Thread consThread = new Thread(new Consumer());
        prodThread.start();
        consThread.start();
        try {
            prodThread.join();
            consThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Producer implements Runnable {
        public void run() {
            int limit = Integer.parseInt(ResourceBundle.getBundle("values")
                    .getString("MaxGeneratedValue"));
            int index = 0;
            try {
                while (index <= limit) {
                    System.out.println("Producer thread generating: " + index +
                            " " + LocalTime.now());
                    bQueue.put(index);
                    Thread.sleep(50);
                    index++;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class Consumer implements Runnable {
        public void run() {
            int limit = Integer.parseInt(ResourceBundle.getBundle("values")
                    .getString("MaxGeneratedValue"));
            try {
                while (true) {
                    int value = bQueue.take();
                    System.out.println("Consumer thread consuming: " + value
                            + " " + LocalTime.now());
                    Thread.sleep(50);
                    if (value == limit)
                        break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
