package com.epam.training.model.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalTime;

public class CustomRwLockTest {
    private final Logger logger = LogManager.getLogger(CustomRwLockTest.class.getName());
    private static final CustomReadWriteLock lock = new CustomReadWriteLock();
    private final int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private int sqr = 2;


    public CustomRwLockTest() {
    }

    public class FirstThread implements Runnable {
        @Override
        public void run() {
            try {
                methodInc();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public class SecondThread implements Runnable {
        @Override
        public void run() {
            try {
                readInc();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public class ThirdThread implements Runnable {
        @Override
        public void run() {
            try {
                methodSqr();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runThreads() {
        Thread first = new Thread(new CustomRwLockTest.FirstThread());
        Thread second = new Thread(new CustomRwLockTest.SecondThread());
        Thread third = new Thread(new CustomRwLockTest.ThirdThread());
        first.start();
        second.start();
        third.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            logger.warn("Interrupted thread");
        }

    }

    private void methodInc() throws InterruptedException {
        lock.lockRead();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        for (int value : array) {
            logger.info(Thread.currentThread().getName() + " -> " + value + " |" + LocalTime.now());
        }
        lock.unlockRead();
    }

    private void readInc() throws InterruptedException {
        lock.lockRead();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        for (int value : array) {
            logger.info(Thread.currentThread().getName() + " -> " + value + " |" + LocalTime.now());
        }
        lock.unlockRead();
    }

    private void methodSqr() throws InterruptedException {
        lock.lockWrite();
        logger.info(Thread.currentThread().getName() + " started at " + LocalTime.now());
        sqr *= sqr;
        logger.info(Thread.currentThread().getName() + " sqr");
        logger.info(Thread.currentThread().getName() + " -> " + sqr + " |" + LocalTime.now());
        lock.unlockWrite();
    }
}
