package com.epam.training.model.task3;


public class CustomReadWriteLock {

    private int readCount;
    private int writeCount;
    private int writeRequests;

    public synchronized void lockRead() throws InterruptedException {
        while (writeCount > 0 || writeRequests > 0) {
            wait();
        }
        readCount++;
    }

    public synchronized void unlockRead() {
        readCount--;
        notifyAll();
    }

    public synchronized void lockWrite() throws InterruptedException {
        writeRequests++;
        while (readCount > 0 || writeCount > 0) {
            wait();
        }
        writeRequests--;
        writeCount++;
    }

    public synchronized void unlockWrite() throws InterruptedException {
        writeCount--;
        notifyAll();
    }
}
