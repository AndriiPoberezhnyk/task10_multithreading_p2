package com.epam.training.view;

import com.epam.training.model.task1.ThreadLockTesting;
import com.epam.training.model.task2.BlockingQueueHW;
import com.epam.training.model.task3.CustomRwLockTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private Locale locale;
    private ResourceBundle bundle;

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();


    }

    public void run(){
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::method1);
        methodsMenu.put("2", this::method2);
        methodsMenu.put("3", this::method3);

        outputMenu();
    }

    private void method1(){
        new ThreadLockTesting().runThreeThreads();
    }

    private void method2(){
        new BlockingQueueHW().runThreads();
    }

    private void method3(){
        new CustomRwLockTest().runThreads();
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
